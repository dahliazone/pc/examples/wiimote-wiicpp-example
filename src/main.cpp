// Simple graphics functions
#include "sdlgfx.hpp"

// For semi-easy string manipulation
#include "string_convert.hpp"

// WiiCpp-library
#include "wiicpp.h"

// Standard scheisse
#include <iostream>
#include <stdexcept>
#include <string>
#include <cstdlib>


////////////////////////////////
// Some ugly globals!
////////////////////////////////
// Framebuffer & font & sprites
SDL_Surface *screen;
TTF_Font* font;
SDL_Surface* sprite1 = 0;
SDL_Surface* sprite2 = 0;

// Some positions for objects
Point2dReal position1(200, 200);
Point2dReal velocity1(0, 0);

Point2dReal position2(400, 250);
Point2dReal velocity2(0, 0);

// Wiimote stuff
CWii *wii;
bool reloadWiimotes = false;
float old_pitch = 0, old_roll = 0, old_yaw = 0, old_a_pitch = 0, old_a_roll = 0;
float new_pitch = 0, new_roll = 0, new_yaw = 0, new_a_pitch = 0, new_a_roll = 0;
float delta_pitch = 0, delta_roll = 0, delta_yaw = 0, delta_a_pitch = 0, delta_a_roll = 0;


// Slow ugly pixel effect
void PixelEffect(SDL_Surface* screen, int step)
{
	for (int y=0; y<screen->h; y++) {
		for (int x=0; x<screen->w; x++) {
			DrawPixel(screen, Point2d(x, y), RgbaColor((x*x)/256+3*y+step, (y*y)/256+x+step, step));
		}
	}
}

// Prints out wiimote info
void PrintWiimoteInfo(int wiimoteIndex, SDL_Surface* screen, float deltaTime, float elapsedTime)
{
	// Get list of wiimotes (no regeneration)
	std::vector<CWiimote>& wiimotes = wii->GetWiimotes(0);

	// Get wiimote
	CWiimote& wm = wiimotes[wiimoteIndex];

	std::string str;
	int yPos = 5;

	// Accelerometer?
	if (wm.isUsingACC()) {
		// Get accelerometer data
		float pitch, roll, yaw, a_pitch, a_roll;
		wm.Accelerometer.GetOrientation(pitch, roll, yaw);
		wm.Accelerometer.GetRawOrientation(a_pitch, a_roll);

		// Print orientation
		str = "Orientation:";
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  pitch=";
		StringAdd(str, pitch);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  roll=";
		StringAdd(str, roll);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  yaw=";
		StringAdd(str, yaw);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 25;

		// Print raw orientation
		str = "Raw Orientation:";
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  a_pitch=";
		StringAdd(str, pitch);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  a_roll=";
		StringAdd(str, roll);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 35;
	}

	// Motion plus?
	if (wm.isUsingMotionPlus()) {
		float roll, pitch, yaw;
		wm.ExpansionDevice.MotionPlus.Gyroscope.GetRates(roll, pitch, yaw);

		str = "Motion Plus:";
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  pitch=";
		StringAdd(str, pitch);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  roll=";
		StringAdd(str, roll);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  yaw=";
		StringAdd(str, yaw);
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 25;

		/*
		std::cout << "motion plus roll rate = " << roll_rate << std::endl;
		std::cout << "motion plus pitch rate = " << pitch_rate << std::endl;
		std::cout << "motion plus yaw rate = " << yaw_rate << std::endl;
		*/
	}

	// IR tracking?
	if (wm.isUsingIR()) {
		std::vector<CIRDot>::iterator i;
		int x, y;
		int index;

		str = "IR tracking";
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  Num IR Dots: ";
		StringAdd(str, wm.IR.GetNumDots());
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;

		str = "  IR State: ";
		StringAdd(str, wm.IR.GetState());
		DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
		yPos += 15;


		std::vector<CIRDot>& dots = wm.IR.GetDots();

		for(index = 0, i = dots.begin(); i != dots.end(); ++index, ++i)
		{
			if((*i).isVisible())
			{
				(*i).GetCoordinate(x, y);

				str = "  IR source: ";
				StringAdd(str, index);
				str += " (";
				StringAdd(str, x);
				str += " ";
				StringAdd(str, y);
				str += ")";
				DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
				yPos += 15;

				wm.IR.GetCursorPosition(x, y);
				str = "  IR cursor: (";
				StringAdd(str, x);
				str += " ";
				StringAdd(str, y);
				str += ")";
				DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
				yPos += 15;

				str = "  IR z distance: ";
				StringAdd(str, wm.IR.GetDistance());
				DrawText(screen, Point2d(0, yPos), str, font, Rgbacolor(255, 255, 255), Rgbacolor(0, 0, 0), solid);
				yPos += 15;
			}
		}
	}
}


// Draws everything on screen
void DrawScreen(SDL_Surface* screen, float deltaTime, float elapsedTime)
{
	// Lock surface
	if (SDL_MUSTLOCK(screen)) {
		if(SDL_LockSurface(screen) < 0) return;
	}

	// Clear screen
	//SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
	PixelEffect(screen, (int)(elapsedTime*100.0f)); // lags the wiimote input a lot...
	PixelEffect(screen, (int)(elapsedTime*100.0f)); // lags the wiimote input a lot...

	// Prints out wiimote info for the first wiimote (index=0)
	PrintWiimoteInfo(0, screen, deltaTime, elapsedTime);

	// Draw sprites
	DrawSurfaceCentered(screen, sprite1, Point2d(position1.x, position1.y));
	DrawSurfaceCentered(screen, sprite2, Point2d(position2.x, position2.y));

	// Unlock surface
	if (SDL_MUSTLOCK(screen))
		SDL_UnlockSurface(screen);

	// Show the framebuffer on screen
	SDL_Flip(screen);
}

// Initializes wiimotes
void InitWiimotes()
{
	wii = new CWii(); // Defaults to 4 remotes
	std::vector<CWiimote>::iterator i;
	int numFound;
	int index;

	std::cout << "Searching for wiimotes... Turn them on!" << std::endl;

	//Find the wiimote
	numFound = wii->Find(5);

	std::cout << "Found "<<numFound<<" Wiimotes" << std::endl;
	// Connect to the Wiimotes
	std::vector<CWiimote>& wiimotes = wii->Connect();

	std::cout << "Connected to "<<wiimotes.size()<<" wiimotes" << std::endl;

	// Writing to the Wiimotes
	for (index = 0, i = wiimotes.begin(); i != wiimotes.end(); ++i, ++index) {
		CWiimote & wiimote = *i;

		//Set Leds
		int LED_MAP[4] = {CWiimote::LED_1, CWiimote::LED_2, CWiimote::LED_3, CWiimote::LED_4};
		wiimote.SetLEDs(LED_MAP[index]);

		//Rumble for 0.2 seconds as a connection ack
		wiimote.SetRumbleMode(CWiimote::ON);
		usleep(200000);
		wiimote.SetRumbleMode(CWiimote::OFF);

		// Receive information about accelerometers
		wiimote.SetMotionSensingMode(CWiimote::ON);
		wiimote.IR.SetMode(CIR::ON);
		//wiimote.EnableMotionPlus(CWiimote::ON);
	}
}

// De-initializes wiimotes
void DeInitWiimotes() {
	delete wii;
}


// Handler for wiimote events
void HandleEvent(CWiimote &wm)
{
	// MotionPlus
	if (wm.Buttons.isJustPressed(CButtons::BUTTON_PLUS)) {
		wm.EnableMotionPlus(CWiimote::ON);
	}
	if (wm.Buttons.isJustPressed(CButtons::BUTTON_MINUS)) {
		wm.EnableMotionPlus(CWiimote::OFF);
	}

	// Rumble
	if (wm.Buttons.isJustPressed(CButtons::BUTTON_HOME)) {
		wm.ToggleRumble();
	}

	if (wm.isUsingMotionPlus()) {
		wm.ExpansionDevice.MotionPlus.Gyroscope.GetRates(new_roll, new_pitch, new_yaw);

		const float SLOWNESS = 1.0f;
		delta_yaw += (new_yaw - old_yaw) * SLOWNESS;
		delta_roll += (new_roll - old_roll) * SLOWNESS;
		delta_pitch += (new_pitch - old_pitch) * SLOWNESS;
		delta_a_pitch += (new_a_pitch - old_a_pitch) * SLOWNESS;
		delta_a_roll += (new_a_roll - old_a_roll) * SLOWNESS;

		old_pitch = new_pitch;
		old_roll = new_roll;
		old_yaw = new_yaw;
		old_a_pitch = new_a_pitch;
		old_a_roll = new_a_roll;
	}
	else {//if (wm.isUsingACC()) {
		wm.Accelerometer.GetOrientation(new_pitch, new_roll, new_yaw);
		wm.Accelerometer.GetRawOrientation(new_a_pitch, new_a_roll);

		const float SLOWNESS = 1.0f;
		delta_yaw += (new_yaw - old_yaw) * SLOWNESS;
		delta_roll += (new_roll - old_roll) * SLOWNESS;
		delta_pitch += (new_pitch - old_pitch) * SLOWNESS;
		delta_a_pitch += (new_a_pitch - old_a_pitch) * SLOWNESS;
		delta_a_roll += (new_a_roll - old_a_roll) * SLOWNESS;

		old_pitch = new_pitch;
		old_roll = new_roll;
		old_yaw = new_yaw;
		old_a_pitch = new_a_pitch;
		old_a_roll = new_a_roll;
	}
}

// Handler for wiimote status
void HandleStatus(CWiimote &wm)
{
	std::cout << std::endl;
	std::cout << "--- CONTROLLER STATUS [wiimote id "<<wm.GetID()<<"] ---" << std::endl << std::endl;

	std::cout << "attachment: "<<wm.ExpansionDevice.GetType() << std::endl;
	std::cout << "speaker: "<<wm.isUsingSpeaker() << std::endl;
	std::cout << "ir: "<<wm.isUsingIR() << std::endl;
	std::cout << "leds: "<<wm.isLEDSet(1)<<" "<<wm.isLEDSet(2)<<" "<<wm.isLEDSet(3)<<" "<<wm.isLEDSet(4) << std::endl;
	std::cout << "battery: "<<wm.GetBatteryLevel()<<" %%"  << std::endl;
}

// Handler for wiimote disconnects
void HandleDisconnect(CWiimote &wm)
{
	std::cout << std::endl;
	std::cout << "--- DISCONNECTED [wiimote id "<<wm.GetID()<<"] ---" << std::endl;
	std::cout << std::endl;
}

// Handler for wiimote read data
void HandleReadData(CWiimote &wm)
{
	std::cout << std::endl;
	std::cout << "--- DATA READ [wiimote id "<<wm.GetID()<<"] ---"  << std::endl;
	std::cout << std::endl;
}

// Function that updates wiimote state
void UpdateWiimotes()
{
	// Get list of wiimotes (no regeneration)
	std::vector<CWiimote>& wiimotes = wii->GetWiimotes(0);

	if (reloadWiimotes == true) {
		// Regenerate the list of wiimotes
		wiimotes = wii->GetWiimotes(1);
		reloadWiimotes = false;
	}

	//Poll the wiimotes to get the status like pitch or roll
	while (wii->Poll()) {
		for (std::vector<CWiimote>::iterator i = wiimotes.begin(); i != wiimotes.end(); ++i) {
			// Use a reference to make working with the iterator handy.
			CWiimote& wiimote = *i;
			switch(wiimote.GetEvent())
			{
			case CWiimote::EVENT_EVENT:
				HandleEvent(wiimote);
				break;

			case CWiimote::EVENT_STATUS:
				HandleStatus(wiimote);
				break;

			case CWiimote::EVENT_DISCONNECT:
			case CWiimote::EVENT_UNEXPECTED_DISCONNECT:
				HandleDisconnect(wiimote);
				reloadWiimotes = true;
				break;

			case CWiimote::EVENT_READ_DATA:
				HandleReadData(wiimote);
				break;

			case CWiimote::EVENT_MOTION_PLUS_INSERTED:
				std::cout << "Motion Plus inserted." << std::endl;
				break;

			case CWiimote::EVENT_NUNCHUK_REMOVED:
			case CWiimote::EVENT_CLASSIC_CTRL_REMOVED:
			case CWiimote::EVENT_GUITAR_HERO_3_CTRL_REMOVED:
			case CWiimote::EVENT_MOTION_PLUS_REMOVED:
				std::cout << "An expansion was removed." << std::endl;
				HandleStatus(wiimote);
				reloadWiimotes = true;
				break;

			default:
				break;
			}
		}
	}

	// Move sprite 1
	CWiimote& wm = wiimotes[0];
	float speed = 200.0f;
	if (wm.Buttons.isHeld(CButtons::BUTTON_UP)) {
		velocity1.y = -speed;
	} else if (wm.Buttons.isHeld(CButtons::BUTTON_DOWN)) {
		velocity1.y = speed;
	} else {
		velocity1.y = 0;
	}

	if (wm.Buttons.isHeld(CButtons::BUTTON_LEFT)) {
		velocity1.x = -speed;
	} else if (wm.Buttons.isHeld(CButtons::BUTTON_RIGHT)) {
		velocity1.x = speed;
	} else {
		velocity1.x = 0;
	}

	// if the Motion Plus is turned on then print angles
	if (wm.isUsingMotionPlus()) {
		float roll_rate, pitch_rate, yaw_rate;
		//wm.ExpansionDevice.MotionPlus.Gyroscope.GetRates(roll_rate,pitch_rate,yaw_rate);
/*
		std::cout << "motion plus roll rate = " << roll_rate << std::endl;
		std::cout << "motion plus pitch rate = " << pitch_rate << std::endl;
		std::cout << "motion plus yaw rate = " << yaw_rate << std::endl;
		*/
	}

	// Move sprite 2
	if (wm.isUsingMotionPlus()) {
		velocity2.x += -delta_yaw;
		velocity2.y += delta_pitch;
	} else {
		velocity2.x += delta_pitch;
		velocity2.y += delta_roll;
	}

	// clear delta values
	delta_pitch = 0;
	delta_roll = 0;
	delta_yaw = 0;
	delta_a_pitch = 0;
	delta_a_roll = 0;
}

// Function that updates everything
bool UpdateStuff(float deltaTime, float elapsedTime)
{
	// Update wiimotes
	UpdateWiimotes();

	// Handle SDL events
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			return false;
			break;

		case SDL_KEYUP:
			switch(event.key.keysym.sym){
			default:
				;
			}
			break;
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym){
			case SDLK_ESCAPE:
				return false;
				break;
			default:
				;
			}
			break;
		}
	}

	position1.x += velocity1.x * deltaTime;
	position1.y += velocity1.y * deltaTime;
	if (position1.x < 0)
		position1.x = 0;
	if (position1.y < 0)
		position1.y = 0;
	if (position1.x > screen->w-1)
		position1.x = screen->w-1;
	if (position1.y > screen->h-1)
		position1.y = screen->h-1;

	position2.x += velocity2.x * deltaTime;
	position2.y += velocity2.y * deltaTime;
	if (position2.x < 0)
		position2.x = 0;
	if (position2.y < 0)
		position2.y = 0;
	if (position2.x > screen->w-1)
		position2.x = screen->w-1;
	if (position2.y > screen->h-1)
		position2.y = screen->h-1;

	static int lastFpsCheck = 0;
	if ((int)elapsedTime+1 > lastFpsCheck) {
		lastFpsCheck = (int)elapsedTime + 1;
		std::cout << (1.0f / deltaTime) << " FPS" << std::endl;
	}

	return true;
}


// Main program & main loop
int main(int argc, char* argv[])
{
	bool quitting = false;

	// Init wiimotes
	InitWiimotes();

	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0)
		return EXIT_FAILURE;

	int returnValue = EXIT_SUCCESS;
	try {
		// Set video mode
		if (!(screen = SDL_SetVideoMode(800, 600, 32, SDL_HWSURFACE))) {
			throw std::runtime_error(SDL_GetError());
		}

		// Load sprites
		std::string filename = "data/holywater.png";
		sprite1 = LoadImage(filename);
		if (sprite1 == 0)
			throw std::runtime_error("File not found! \""+filename+"\"");
		filename = "data/holyshit.png";
		sprite2 = LoadImage(filename);
		if (sprite2 == 0)
			throw std::runtime_error("File not found! \""+filename+"\"");

		// Init and load font
		InitFonts();
		font = Loadfont("data/DroidSansMono.ttf", 16);

		// Variables for timing & FPS calculation
		Uint32 oldTime = SDL_GetTicks(), currentTime = SDL_GetTicks();
		float deltaTime = 0.0f, elapsedTime = 0.0f;

		// Main loop
		while (quitting == false)
		{
			// Calculate timing stuff
			oldTime = currentTime;
			currentTime = SDL_GetTicks();
			deltaTime = (currentTime - oldTime) / 1000.0f;
			elapsedTime += deltaTime;

			// Update from input & calculate new positions
			if (UpdateStuff(deltaTime, elapsedTime) == false)
				quitting = true;

			// Draw everything
			DrawScreen(screen, deltaTime, elapsedTime);

			// Wait a while so we don't hog the whole CPU
			SDL_Delay(10);
		}

	} catch (std::exception const& e) {
		std::cerr<< "Aborting! " << e.what() << std::endl;
		returnValue = EXIT_FAILURE;
	} catch (...) {
		std::cerr << "Aborting! Unknown exception!" << std::endl;
		returnValue = EXIT_FAILURE;
	}

	// De-init everything
	DeInitWiimotes();
	SDL_Quit();

	return returnValue;
}
