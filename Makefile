all:
	scons -Q -j3 nocolors=1
clean:
	scons -c nocolors=1
	distclean: clean
	realclean: clean

.PHONY: all clean distclean realclean
