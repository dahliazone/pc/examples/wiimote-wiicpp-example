-= Wiimote example application with WiiCpp =-

Prequisities:
- Wiimote
- Bluetooth hardware & BlueZ stack (ubuntu has it) & Bluetooth turned ON
- Linux (tested with ubuntu 10.04 32-bit) (might work on OS X too...)
- GCC G++ (C++ compiler) & make
- scons
- cmake
- libbluetooth-dev
- libsdl1.2-dev
- libsdl-image1.2-dev
- libsdl-ttf2.0-dev
- optionally: Qt Creator as the IDE 
  - project files are made with version 2.0.94 (2.1.0 rc1)


How to compile WiiC/WiiCpp (only command-line):
 cd lib/wiic
 mkdir build
 cd build
 # generates the makefile
 cmake ../src
 # compiles the binaries into the build-directory
 make
 cd ../../..


How to compile the example (command-line):
 scons

How to run (command-line):
 ./wiimote
 # a shell script that sets LD_LIBRARY_PATH and runs ./bin/binary


How to compile the example (Qt Creator):
 Remember to compile WiiC using command-line
 Open the project file (wiimote.creator)
 CTRL-b

How to run (Qt Creator):
 CTRL-r
 Choose the file "./wiimote"


How to use the program:
  You must have the bluetooth turned on.
 
  When starting the program hold buttons 1 & 2 on the wiimote to turn on
  the discovery mode.

  D-pad controls the first sprite.
  Turning the wiimote controls the second sprite.
  ESC quits.


And yes, the source code is ugly. Use it as you wish, but remember that:
- SDL, SDL-image, SDL-tff2.0 and WiiC have their own licenses
- Sprites are taken from the net (don't know the author)
- Font is from Android OS and has it's own license

Enjoy,
- Codise / Dahlia
