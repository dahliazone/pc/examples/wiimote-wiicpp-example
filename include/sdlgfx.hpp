#ifndef SDLGFX_HPP
#define SDLGFX_HPP

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include <iostream>
#include <string>
#include <stdexcept>
#include <cmath>

enum textquality { solid, shaded, blended };

typedef struct Point2d {
public:
	inline Point2d() : x(0), y(0) {}
	inline Point2d(int x, int y) : x(x), y(y) {}

	int x;
	int y;
} Point2d;

typedef struct Point2dReal {
public:
	inline Point2dReal() : x(0), y(0) {}
	inline Point2dReal(float x, float y) : x(x), y(y) {}

	float x;
	float y;
} Point2dReal;

typedef struct RgbaColor {
public:
	inline RgbaColor() : r(0), g(0), b(0), a(0) {}
	inline RgbaColor(char r, char g, char b, char a=0) : r(r), g(g), b(b), a(a) {}

	char r;
	char g;
	char b;
	char a;
} Rgbacolor;


void InitFonts();
TTF_Font* Loadfont(std::string filename, int pointSize);
SDL_Surface* CreateText(TTF_Font *fonttodraw, const Rgbacolor& fg, Rgbacolor bg, std::string text, textquality quality);
void DrawText(SDL_Surface *screen, const Point2d& pos, std::string text, TTF_Font *font, const Rgbacolor& fg, Rgbacolor bg, textquality quality);
void DrawPixel(SDL_Surface *screen, const Point2d& pos, const Rgbacolor& color);
Uint32 GetPixel(SDL_Surface* buffer, const Point2d& pos);
void DrawCircle(SDL_Surface *screen, const Point2d& pos, const Uint8 radius, const Uint8 bpp, const Rgbacolor& color);
void DrawRect(SDL_Surface *buffer, const Point2d& pos, int w, int h, const Rgbacolor& color);
void DrawMarker(SDL_Surface *buffer, const Point2d& pos, int size, const Rgbacolor& color);
SDL_Surface* LoadImage(std::string filename);
bool DrawSurfaceCentered(SDL_Surface* dest, SDL_Surface* src, const Point2d& pos);
bool DrawSurface(SDL_Surface* dest, SDL_Surface* src, const Point2d& pos);




inline void InitFonts()
{
	if (TTF_Init() == -1) {
		throw std::runtime_error("Unable to initialize SDL_ttf: " + std::string(TTF_GetError()));
	}
}


inline TTF_Font* Loadfont(std::string filename, int pointSize)
{
	TTF_Font* tmpfont;
	tmpfont = TTF_OpenFont(filename.c_str(), pointSize);
	if (tmpfont == NULL)
		throw std::runtime_error("Unable to load font: \""+filename+"\" - "+std::string(TTF_GetError()));
	return tmpfont;
}


inline SDL_Surface* CreateText(TTF_Font *fonttodraw, const Rgbacolor& fg, Rgbacolor bg, std::string text, textquality quality)
{
  SDL_Color tmpfontcolor = { fg.r,fg.g,fg.b,fg.a };
  SDL_Color tmpfontbgcolor = { bg.r, bg.g, bg.b, bg.a };
  SDL_Surface *resulting_text;

  if (quality == solid)
	  resulting_text = TTF_RenderText_Solid(fonttodraw, text.c_str(), tmpfontcolor);
  else if (quality == shaded)
	  resulting_text = TTF_RenderText_Shaded(fonttodraw, text.c_str(), tmpfontcolor, tmpfontbgcolor);
  else if (quality == blended)
	  resulting_text = TTF_RenderText_Blended(fonttodraw, text.c_str(), tmpfontcolor);

  return resulting_text;
}

inline void DrawText(SDL_Surface *screen, const Point2d& pos, std::string text, TTF_Font *font, const Rgbacolor& fg, Rgbacolor bg, textquality quality)
{
	SDL_Surface* textSurface = CreateText(font, fg, bg, text, quality);
	DrawSurface(screen, textSurface, pos);
	SDL_FreeSurface(textSurface);
}

inline void DrawPixel(SDL_Surface *screen, const Point2d& pos, const Rgbacolor& color)
{
	Uint32 colorValue = SDL_MapRGB(screen->format, color.r, color.g, color.b);

	switch (screen->format->BytesPerPixel) {
	case 1: { /* Assuming 8-bpp */
		Uint8 *bufp;

		bufp = (Uint8 *)screen->pixels + pos.y*screen->pitch + pos.x;
		*bufp = colorValue;
	}
		break;

	case 2: { /* Probably 15-bpp or 16-bpp */
		Uint16 *bufp;

		bufp = (Uint16 *)screen->pixels + pos.y*screen->pitch/2 + pos.x;
		*bufp = colorValue;
	}
		break;

	case 3: { /* Slow 24-bpp mode, usually not used */
		Uint8 *bufp;

		bufp = (Uint8 *)screen->pixels + pos.y*screen->pitch + pos.x;
		*(bufp+screen->format->Rshift/8) = color.r;
		*(bufp+screen->format->Gshift/8) = color.g;
		*(bufp+screen->format->Bshift/8) = color.b;
	}
		break;

	case 4: { /* Probably 32-bpp */
		Uint32 *bufp;

		bufp = (Uint32 *)screen->pixels + pos.y*screen->pitch/4 + pos.x;
		*bufp = colorValue;
	}
		break;
	}
}


inline Uint32 GetPixel(SDL_Surface* buffer, const Point2d& pos)
{
	Uint32 col = 0;
	char* index = (char*)buffer->pixels;
	index += (buffer->pitch * pos.y);
	index += (buffer->format->BytesPerPixel * pos.x);
	memcpy (&col, index, buffer->format->BytesPerPixel);
	//SDL_GetRGB(col, buffer->format, &color.r, &color.g, &color.b);
	return col;
}


inline void DrawCircle(SDL_Surface *screen, const Point2d& pos, const Uint8 radius, const Uint8 bpp, const Rgbacolor& color)
{
	Uint32 color1 = SDL_MapRGB(screen->format, color.r, color.g, color.b);
	Uint32 *bufp1, *bufp2;
	Uint16 rsqr = radius*radius, tx, tx1, tx2, ty, ty1, ty2;
	float tmpy, tmpx;

	tmpx = -ceil(radius / sqrt((float)2));
	while(tmpx <= (float)radius)
	{
		if( (float)fabs(tmpx) < (float)radius/sqrt((float)2)){
			tmpy = sqrt((float)(rsqr - pow(tmpx,2)));
			ty1 = pos.y + (Uint16)floor(tmpy+0.5);
			ty2 = pos.y - (Uint16)floor(tmpy+0.5);
			tx = pos.x-(Uint16) tmpx;
			bufp1 = (Uint32 *)screen->pixels + ty1*screen->pitch/bpp + tx;
			bufp2 = (Uint32 *)screen->pixels + ty2*screen->pitch/bpp + tx;
			*bufp1 = color1;
			*bufp2 = color1;
		}
		tmpx++;
	}
	tmpy = -ceil(radius / sqrt((float)2));
	while(tmpy <= (float)radius)
	{
		if( (float)fabs(tmpy) < (float)radius/sqrt((float)2)){
			tmpx = sqrt((float)(rsqr - pow(tmpy,2)));
			tx1 = pos.x + (Uint16)floor(tmpx+0.5);
			tx2 = pos.x - (Uint16)floor(tmpx+0.5);
			ty = pos.y-(Uint16) tmpy;
			bufp1 = (Uint32 *)screen->pixels + ty*screen->pitch/bpp + tx1;
			bufp2 = (Uint32 *)screen->pixels + ty*screen->pitch/bpp + tx2;
			*bufp1 = color1;
			*bufp2 = color1;
		}
		tmpy++;
	}

}


inline void DrawRect(SDL_Surface *buffer, const Point2d& pos, int w, int h, const Rgbacolor& color)
{
	for (int i=pos.x; i<pos.x+w; i++) {
		if (i >= 0 && i < buffer->w) {
			if (pos.y >= 0 &&pos.y < buffer->h)
				DrawPixel(buffer, Point2d(i, pos.y), color);
			if (pos.y+h >= 0 &&pos.y+h < buffer->h)
				DrawPixel(buffer, Point2d(i, pos.y+h), color);
		}
	}

	for (int i=pos.y; i<pos.y+h; i++) {
		if (i >= 0 && i < buffer->h) {
			if (pos.x >= 0 &&pos.x < buffer->w)
				DrawPixel(buffer, Point2d(pos.x, i), color);
			if (pos.x+w >= 0 &&pos.x+w < buffer->w)
				DrawPixel(buffer, Point2d(pos.x+w, i), color);
		}
	}
}


inline void DrawMarker(SDL_Surface *buffer, const Point2d& pos, int size, const Rgbacolor& color)
{
	int size2 = size / 2;
	for (int i=-size2; i<size2; i++)
		if (pos.x+i >= 0 && pos.y >= 0 && pos.x+i < buffer->w && pos.y < buffer->h)
			DrawPixel(buffer, Point2d(pos.x+i, pos.y), color);

	for (int i=-size2; i<size2; i++)
		if (pos.x >= 0 && pos.y+i >= 0 && pos.x < buffer->w && pos.y+i < buffer->h)
			DrawPixel(buffer, Point2d(pos.x, pos.y+i), color);
}


inline SDL_Surface* LoadImage(std::string filename)
{
	SDL_Surface* temp = 0;
	SDL_Surface* image = 0;

	if ((temp = IMG_Load(filename.c_str())) == 0) {
		return 0;
	}

	image = SDL_DisplayFormatAlpha(temp);
	SDL_FreeSurface(temp);

	return image;
}


inline bool DrawSurfaceCentered(SDL_Surface* dest, SDL_Surface* src, const Point2d& pos)
{
	if (dest == 0 || src == 0) {
		return false;
	}

	SDL_Rect destRect;
	destRect.x = pos.x - src->w / 2;
	destRect.y = pos.y - src->h / 2;

	SDL_BlitSurface(src, 0, dest, &destRect);

	return true;
}


inline bool DrawSurface(SDL_Surface* dest, SDL_Surface* src, const Point2d& pos)
{
	if (dest == 0 || src == 0) {
		return false;
	}

	SDL_Rect destRect;
	destRect.x = pos.x;
	destRect.y = pos.y;

	SDL_BlitSurface(src, 0, dest, &destRect);

	return true;
}


#endif // SDLGFX_HPP
