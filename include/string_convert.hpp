/**
 * @file string_convert.hpp
 * @brief Template functions for converting from arbitrary type to string and vice versa.
 * @author Max Vilkki [Codise / Dahlia]
 */

#ifndef __STRING_CONVERT_HPP__
#define __STRING_CONVERT_HPP__

#include <sstream>
#include <string>


/**
 * Template function for converting an arbitrary type to C++ string
 */
template <class T>
void StringAdd(std::string &val, const T &t) {
	std::ostringstream oss; // create a stream
	oss << t;          // insert value to stream
	val += oss.str();  // extract string and copy
}


/**
 * Template function for converting from one arbitrary type to another
 */
template <class out_type, class in_value>
out_type CastStream(const in_value & t) {
	std::stringstream ss;
	ss << t;         // first insert value to stream
	out_type result; // value will be converted to out_type
	ss >> result;    // write value to result
	return result;
}


#endif // End header guard

